import { NgModule } from "@angular/core";
import { RouterModule, Routes, provideRouter, withHashLocation } from "@angular/router";
import { LoginComponent } from "./pages/login/login.component";
import { HomeComponent } from "./pages/home/home.component";
import { NgxAuthenticationService } from "ngx-authentication";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { FileInputComponent } from "./pages/file-input/file-input.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [NgxAuthenticationService] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [NgxAuthenticationService] },
  { path: 'file-input', component: FileInputComponent, canActivate: [NgxAuthenticationService] },
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
  providers: [provideRouter(routes, withHashLocation())]
})
export class AppRoutes { }