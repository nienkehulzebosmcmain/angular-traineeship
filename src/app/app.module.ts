import { ErrorHandler, NgModule, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { SelectBoxComponent } from "./components/select-box/select-box.component";
import { HTTP_INTERCEPTORS, provideHttpClient, withInterceptorsFromDi } from "@angular/common/http";
import { NgOptimizedImage } from "@angular/common";
import { LoginComponent } from './pages/login/login.component';
import { AppRoutes } from './app-routes';
import { HeaderComponent } from './pages/header/header.component';
import { HomeComponent } from './pages/home/home.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthInterceptor } from 'ngx-api';
import { ErrorMessageService } from './error/error-message.service';
import { provideCharts, withDefaultRegisterables } from 'ng2-charts';
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { TextFieldComponent } from './components/textfield/textfield.component';
import { DashboardCardComponent } from "./components/dashboard/dashboard-card/dashboard-card.component";
import { FileInputComponent } from './pages/file-input/file-input.component';
import { ByPassSanitizerPipe } from './components/dialog/sanitizer.pipe';
import { DialogComponent } from './components/dialog/dialog.component';
import { ServiceWorkerModule } from '@angular/service-worker';

@NgModule({
    declarations: [
        AppComponent,
        SelectBoxComponent,
        LoginComponent,
        HeaderComponent,
        HomeComponent,
        DashboardComponent,
        TextFieldComponent,
        FileInputComponent,
        DialogComponent,
        ByPassSanitizerPipe
    ],
    bootstrap: [AppComponent],
    imports: [BrowserModule,
        NgOptimizedImage,
        AppRoutes,
        ReactiveFormsModule,
        DashboardCardComponent,
        ServiceWorkerModule.register("ngsw-worker.js", {
            enabled: !isDevMode(),
            // Register the ServiceWorker as soon as the application is stable
            // or after 30 seconds (whichever comes first).
            registrationStrategy: 'registerWhenStable:30000',
            scope: '/'
        })],
    providers: [{
        provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptor,
        multi: true
    },
    {
        provide: ErrorHandler,
        useClass: ErrorMessageService
    },
    provideCharts(withDefaultRegisterables()), provideHttpClient(withInterceptorsFromDi())]
})
export class AppModule { }
