import { Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import { DialogService } from "../../components/dialog/dialog.service";

@Component({
  selector: 'file-input',
  templateUrl: 'file-input.component.html',
  styleUrl: 'file-input.component.css'
})
export class FileInputComponent implements OnInit {
  @ViewChild('input', {read: ElementRef} ) input!: ElementRef; 

  constructor(private dialogService: DialogService) { }

  ngOnInit(): void {
    this.dialogService.registerWebWorker();

    this.dialogService.resetFileInputEvent.subscribe(() => {
      this.input.nativeElement.value = '';
    })
  }

  public fileSelected(event: Event) {
    const file: File = (event.target as HTMLInputElement).files![0];

    this.dialogService.postMessage(file);
  }
}