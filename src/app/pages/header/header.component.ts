import { Component, OnInit } from "@angular/core";
import { NgxApiService } from "ngx-api";

@Component({
    selector: 'header',
    templateUrl: './header.component.html',
    styleUrl: './header.component.css'
})
export class HeaderComponent implements OnInit {
    // true = online, false = offline
    public networkStatus: boolean = true;
    
    constructor(private apiService: NgxApiService) { }

    ngOnInit(): void {
        this.apiService.networkStatusEmitter.subscribe((value: boolean) => {
            this.networkStatus = value;
        })
    }
 }