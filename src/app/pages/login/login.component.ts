import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { LoginService } from "./login.service";

@Component({
    selector: 'login',
    templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
    public userForm = new FormGroup({
        apikey: new FormControl('', [Validators.required])
    });

    constructor(public loginService: LoginService) { }

    ngOnInit(): void {
        this.loginService.logout();
    }

    public login() {
        this.loginService.login(this.userForm)
    }

    public isFormValid(): boolean {
        return this.userForm.valid
    }
}