import { Injectable } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { NgxAuthenticationService } from "ngx-authentication";

@Injectable({
    providedIn: 'root'
})
export class LoginService {
    constructor(private router: Router, private _ngxAuthentication: NgxAuthenticationService) { }

    public login(form: FormGroup) {
        if (this._ngxAuthentication.getAuthenticationCookie() == '') {
            this._ngxAuthentication.setAuthenticationCookie(form.value.apikey!);
            this.router.navigate([''])
        }
    }
    
    public logout() {
        this._ngxAuthentication.deleteAuthenticationCookie()
    }
}
