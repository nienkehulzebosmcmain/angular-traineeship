import { Injectable } from "@angular/core";
import { DashboardCardModel } from "./dashboard-models/dashboard-card.model";
import { Observable, Subject, map } from "rxjs";
import { NgxApiService } from "ngx-api";
import { Issue } from "./dashboard-models/issue.model";
import { Iteration } from "./dashboard-models/iteration.model";
import { Participant } from "./dashboard-models/participant.model";

@Injectable({
    providedIn: 'root'
})
export class DashboardService {
    private dashboardData: DashboardCardModel[] = [];
    private dashboardDataSubject = new Subject<DashboardCardModel[]>();

    constructor(public apiService: NgxApiService) { }

    public iterationSelected(newIteration: string) {
        this.dashboardData = [];
        this.refreshDashboardData();
        this.createCharts(newIteration);
    }

    public getDashboardData(): Observable<DashboardCardModel[]> {
        return this.dashboardDataSubject.asObservable();
    }

    private refreshDashboardData() {
        this.dashboardDataSubject.next(this.dashboardData)
    }

    private createCharts(iterationId: string) {
        // Get all issues of iteration
        let issueList: Issue[] = [];
        this.getAllIssuesFromIteration(iterationId)
            .pipe(map((issues: Issue[]) => {
                issues.map((i: Issue) => {
                    const issue: Issue | undefined = this.createIssue(i);
                    if (issue != undefined) {
                        issueList.push(issue);
                    }
                })
            }))
            .subscribe({
                complete: () => {
                    if (issueList.length == 0) { 
                        throw new Error('Iteration has no issues') 
                    }
                    else {
                        // Calculate chart(s)
                        this.calculateBurnDownChart(issueList);

                        if (this.dashboardData.length == 0) {
                            throw new Error("Can't calculate any charts")
                        } else {
                            this.refreshDashboardData();
                        }
                    }
                }
            });
    }

    //#region Helper functions
    private createIssue(issue: Issue) {
        if (issue.id != undefined) {
            return new Issue(
                issue.id,
                issue.state,
                this.deleteTimeFromDate(issue.closed_at),
                issue.weight,
                new Participant(
                    issue.closed_by.id,
                    issue.closed_by.name
                ),
                new Participant(
                    issue.author.id,
                    issue.author.name
                ),
                new Iteration(
                    issue.iteration.id,
                    issue.iteration.state,
                    this.deleteTimeFromDate(issue.iteration.start_date),
                    this.deleteTimeFromDate(issue.iteration.due_date)));
        }
        return undefined;
    }

    // Use new Date() twice to get rid of the time. It interferes with the calculation of days and isn't relevant for the current charts.
    private deleteTimeFromDate(date: Date): Date {
        return new Date(new Date(date).toDateString());
    }

    private calculateDaysBetweenDates(end: Date, start: Date): number {
        return Math.floor((end.getTime() - start.getTime()) / (1000 * 60 * 60 * 24));
    }

    //#endregion

    //#region API calls
    private getAllIssuesFromIteration(iterationId: string): Observable<Issue[]> {
        return this.apiService.get<Issue[]>('https://gitlab.com/api/v4/groups/3564360/issues?iteration_id=' + iterationId + '&page=1&per_page=100');
    }
    //#endregion

    //#region Burndown Chart

    private calculateBurnDownChart(issues: Issue[]) {
        const startDate: Date = issues[0].iteration.start_date;
        const dueDate: Date = issues[0].iteration.due_date;
        const days: number = this.calculateDaysBetweenDates(dueDate, startDate) + 1;

        let dataset = new Array<number>(days);
        let totalWeight: number = 0

        issues = issues.filter((issue: Issue) => {
            if (issue.weight != undefined) {
                totalWeight += issue.weight;
                return true;
            }
            return false;
        })

        // Calculate the days between the due and start date
        // https://stackoverflow.com/questions/4413590/javascript-get-array-of-dates-between-2-dates
        const dateArray = [];
        let currentDate = new Date(startDate);
        while (currentDate <= dueDate) {
            dateArray.push(new Date(currentDate).toDateString());
            // Use UTC date to prevent problems with time zones and DST
            currentDate.setUTCDate(currentDate.getUTCDate() + 1);
        }

        let weightOpen = totalWeight;
        // Check every day - start to finish if any issues closed on that day
        for (let i = 0; i < days; i++) {
            // Calculate days between the closed_at date and the start date to determine whether the issue closed on that specific day
            issues.filter((value: Issue) => {
                const daysToDueDate = this.calculateDaysBetweenDates(value.closed_at, startDate);
                return daysToDueDate == i
            }).forEach((value: Issue) => {
                // If the issue was closed on the specific day then substract the weight of that issue from the total weight that's still open
                weightOpen -= value.weight;
            })
            dataset[i] = weightOpen;
        }

        const averageWeightPerDay: number = totalWeight / days;
        let averageLineDataset = new Array<number>(days);
        for (let i = 1; i <= days; i++) {
            averageLineDataset[i - 1] = Math.round(totalWeight - (averageWeightPerDay * i));
        }

        const participantsCount: number = this.calculateParticipants(issues).length;

        this.dashboardData.push({
            chartName: 'Burndown Chart',
            chartData: {
                datasets: [{
                    label: 'Weight',
                    data: dataset
                }, {
                    label: 'Participants Count',
                    data: new Array<number>(days).fill(participantsCount)
                }, {
                    label: 'Average weight per day',
                    data: averageLineDataset
                }],
                labels: dateArray
            }
            ,
            chartType: 'line'
        })
    }

    private calculateParticipants(issues: Issue[]): Participant[] {
        let participants: Participant[] = [];

        issues.forEach((issue: Issue) => {
            let author: Participant = issue.author;
            let closedBy: Participant = issue.closed_by;

            if (author != undefined) {
                if (participants.find((value: Participant) => value.id == author.id) == undefined) {
                    participants.push(author);
                }
            }

            if (closedBy != undefined) {
                if (participants.find((value: Participant) => value.id == closedBy.id) == undefined) {
                    participants.push(closedBy);
                }
            }
        });

        return participants;
    }

    //#endregion
}