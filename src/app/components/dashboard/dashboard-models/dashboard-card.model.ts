import { ChartType, ChartData, ChartOptions } from 'chart.js';

export type DashboardCardModel = {
    chartName: string,
    chartData: ChartData,
    chartType: ChartType
    chartOptions?: ChartOptions
}
