export class Iteration {
    constructor(public id: string, public state: string, public start_date: Date, public due_date: Date) {}
}