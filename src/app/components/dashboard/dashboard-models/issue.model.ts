import { Iteration } from "./iteration.model";
import { Participant } from "./participant.model";

export class Issue {
    constructor(public id: string, public state: string, public closed_at: Date, public weight: number, public closed_by: Participant, public author: Participant, public iteration: Iteration) {}
}