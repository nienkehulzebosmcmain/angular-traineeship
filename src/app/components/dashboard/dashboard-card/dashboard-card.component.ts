import { Component, Input } from "@angular/core";
import { BaseChartDirective } from "ng2-charts";
import { ChartOptions, ChartType, ChartData } from 'chart.js';

@Component({
    standalone: true,
    imports: [BaseChartDirective],
    selector: 'dashboard-card',
    templateUrl: 'dashboard-card.component.html'
})
export class DashboardCardComponent {
    @Input({required: true}) public chartData!: ChartData;
    @Input({required: true}) public chartType!: ChartType;
    @Input() public chartOptions?: ChartOptions = {};
}