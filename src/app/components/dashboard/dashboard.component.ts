import { Component} from "@angular/core";
import { DashboardService } from "./dashboard.service";
import { DashboardCardModel } from "./dashboard-models/dashboard-card.model";
import { Observable } from "rxjs";

@Component({
    selector: 'dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrl: 'dashboard.component.css'
})
export class DashboardComponent {
    public dashboardCards: Observable<DashboardCardModel[]> = this.dashboardService.getDashboardData();

    constructor(public dashboardService: DashboardService) {}

    public selectOption(option: string) {
        this.dashboardService.iterationSelected(option)
    }
}