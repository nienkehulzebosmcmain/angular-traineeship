import { Dialog } from "@angular/cdk/dialog";
import { EventEmitter, Injectable } from "@angular/core";
import { DialogComponent } from "./dialog.component";
import { NavigationStart, Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class DialogService {
    private worker?: Worker;
    private dialogId: string = 'file-input-dialog';
    public resetFileInputEvent = new EventEmitter();

    constructor(public dialog: Dialog, router: Router) {
        router.events.subscribe((event) => {
            if (event instanceof NavigationStart) {
                this.closeDialog();
            }
        })
     }

    public postMessage(file: File) {
        if (this.worker != undefined) {
            this.worker.postMessage(file);
        }
        else {
            throw new Error("Web Worker not supported");
        }
    }

    public openDialog(src: string) {
        this.dialog.open(DialogComponent, {
            id: this.dialogId,
            data: src,
            closeOnNavigation: true
        });
    }

    public closeDialog() {
        this.dialog.getDialogById(this.dialogId)?.close();
    }

    public registerWebWorker() {
        this.worker = new Worker(new URL('./file-input.worker', import.meta.url));
        this.worker?.addEventListener('message', ({ data }) => {
            this.closeDialog();
            this.openDialog(data)
        });
    }
}