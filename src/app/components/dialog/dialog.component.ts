import { DIALOG_DATA } from "@angular/cdk/dialog";
import { AfterViewInit, Component, ElementRef, HostListener, Inject, ViewChild } from "@angular/core";
import { DialogService } from "./dialog.service";

@Component({
    selector: 'dialog',
    templateUrl: 'dialog.component.html',
    styleUrl: 'dialog.component.css'
})
export class DialogComponent implements AfterViewInit {
    @ViewChild('frame', { read: ElementRef }) frame!: ElementRef;

    @HostListener('window:resize', ['$event.target'])
    onResize(target: any) {
        this.frame.nativeElement.width = target.innerWidth;
        this.frame.nativeElement.height = target.innerHeight;
    }

    constructor(@Inject(DIALOG_DATA) public resourceUrl: string, private dialogService: DialogService) { }

    ngAfterViewInit(): void {
        const img = new Image();
        img.src = this.resourceUrl;
        img.onload = () => {
            this.frame.nativeElement.width = img.width;
            this.frame.nativeElement.height = img.height;
        };
    }

    public close() {
        this.dialogService.closeDialog();
        this.dialogService.resetFileInputEvent.emit();
    }
}