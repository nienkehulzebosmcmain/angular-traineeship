/// <reference lib="webworker" />

addEventListener('message', ({ data }) => {
  if (data instanceof File) {
    postMessage(getObjectUrl(fileToBlob(data)));
  }
});

function fileToBlob(file: File): Blob {
  return new Blob([file], { type: file.type });
}

function getObjectUrl(blob: Blob): string {
  return URL.createObjectURL(blob);
}
