import { Pipe, PipeTransform, SecurityContext } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";

@Pipe({
  name: 'bypasssanitizer'
})
export class ByPassSanitizerPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) { }

  // Make sure to call the method as early as possible and as close as possible to the source of the value, to make it easy to verify no security bug is created by its use.
  transform(url: string): SafeResourceUrl {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}