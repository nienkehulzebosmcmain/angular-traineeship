import { Component, Input, OnInit} from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { debounceTime } from "rxjs";

@Component({
    selector: 'textfield',
    templateUrl: 'textfield.component.html',
    styleUrl: 'textfield.component.css'
})
export class TextFieldComponent implements OnInit {
    @Input({ required: true }) key!: string;
    @Input({ required: true }) fullJsonObject!: Map<string, string>
    @Input({ required: true }) globalForm!: FormGroup
    
    constructor() { }

    ngOnInit(): void {
        const formControl = new FormControl<string>(this.getValue())
        this.globalForm.setControl(this.key, formControl);
        this.globalForm.get(this.key)?.valueChanges
        .pipe(debounceTime(2500)) // Prevent logging and updating of the input with every character change.
        .subscribe({
            next: (value: string) => {
                console.log(`${this.key} has been changed from ${this.getValue()} to ${value}`)
                this.fullJsonObject.set(this.key, value);
            }
        })
    }

    private getValue(): string {
        return this.fullJsonObject.get(this.key) || '';
    }

    public showTextArea() {
        return this.getValue().length > 20 ? true : false;
    }
}