import { NgxApiService } from "ngx-api";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class SelectBoxService {
    constructor(private _ngxApiService: NgxApiService) { }

    public log(value: string): void {
        console.log(value);
        const currentTime = new Date().toLocaleString();
        this.post("https://angular-traineeship-default-rtdb.europe-west1.firebasedatabase.app/selected.json", { [currentTime]: value });
    }

    public get<T>(endpoint: string): Observable<T> {
        return this._ngxApiService.get<T>(endpoint);
    }

    public post(endpoint: string, body: object) {
        this._ngxApiService.post(endpoint, body).subscribe();
    }

    public convertResponseToArray<T>(responseObject: any, key: string): T[] {
        if (key == '') {
            return responseObject as Array<T>;
        }
        else {
            let optionsList: T[] = [];
            let keys: string[] = key.split('.');
            let currentField: undefined;
            for (let i = 0; i <= keys.length - 1; i++) {
                if (i == keys.length - 1) {
                    if (currentField == undefined) {
                        (responseObject as Array<any>).forEach(element => {
                            optionsList.push(element[keys[i]]);
                        })
                    }
                    else {
                        (currentField as Array<any>).forEach(element => {
                            optionsList.push(element[keys[i]]);
                        })
                    }
                }
                else {
                    currentField = responseObject[keys[i]];
                }
            }
            return optionsList;
        }
    }
}
