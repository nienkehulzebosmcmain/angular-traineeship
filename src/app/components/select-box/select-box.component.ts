import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { SelectBoxService } from "./select-box.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";

function setApiUrls(value: { baseApiUrl: '', endpoints: { getOptions: '' } }) {
    return {
        baseApiUrl: value.baseApiUrl,
        endpoints: {
            getOptions: value.baseApiUrl + value.endpoints.getOptions,
        }
    }
}

@Component({
    selector: 'select-box',
    templateUrl: 'select-box.component.html',
    styleUrl: 'select-box.component.css'
})
export class SelectBoxComponent implements OnInit {
    public options: string[] = [];
    public selectBoxForm = new FormGroup({
        selected: new FormControl('', [Validators.required])
    });
    
    @Input({ required: true }) optionsKey!: string;
    @Input({ required: true, transform: setApiUrls }) apiUrls = {
        baseApiUrl: '',
        endpoints: {
            getOptions: '',
        }
    }
    @Output() selected: EventEmitter<string> = new EventEmitter<string>();

    constructor(public selectBoxService: SelectBoxService) { }

    ngOnInit() {
        this.getOptions();
        this.selectBoxForm.controls.selected.valueChanges.subscribe((value: string | null) => {
            this.onSelected(value);
        })
    }

    private getOptions() {
        let optionsList: string[] = [];
        this.selectBoxService.get<Object>(this.apiUrls.endpoints.getOptions).subscribe({
            next: (value: Object) => {
                optionsList = this.selectBoxService.convertResponseToArray<string>(value, this.optionsKey)
            },
            complete: () => {
                this.options = optionsList;
            }
        });
    }

    public onSelected(selected: string | null): void {
        if (selected != null) {
            this.selectBoxService.log(selected);
            this.selected.emit(selected);
        }
        else {
            throw new Error('Selected option is null');
        }
    }
}