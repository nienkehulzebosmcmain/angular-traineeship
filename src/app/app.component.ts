import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgxApiService } from 'ngx-api';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
    public jsonObject: Map<string, string> = new Map<string, string>();
    public formGroup: FormGroup = new FormGroup({});

    constructor(public apiService: NgxApiService) { }

    ngOnInit(): void {
        this.apiService.get<Object>('https://catfact.ninja/fact').subscribe({
            next: (response: Object) => {
                for (const [key, value] of Object.entries(response)) {
                    if (typeof value == 'string') {
                        this.jsonObject.set(key, value);
                    }
                    else if (typeof value == 'number') {
                        this.jsonObject.set(key, value.toString());
                    }
                }
            },
            complete: () => {
                if (this.jsonObject.size == 0) {
                    throw Error("API response could not be converted to textfield(s)")
                }
            }
        });
    }
}
