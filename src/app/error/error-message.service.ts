import { ErrorHandler } from "@angular/core";

export class ErrorMessageService implements ErrorHandler {
    handleError(error: any): void {
        window.alert(error.message);
        console.error(error.message);
    }
}