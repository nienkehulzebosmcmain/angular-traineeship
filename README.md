# Angular Traineeship

## Installatie
Installeer de `ngx-api` library met: `npm install ngx-api-0.0.1.tgz` 

Installeer de `ngx-authentication` library met: `npm install ngx-authentication-0.0.1.tgz`

Om de applicatie te kunnen gebruiken met service workers, gebruik dan: `npm run server`. Dit is een shorthand voor `npx http-server -p 4200 -c-1 dist/angulartraineeship/browser`

## Selectbox Component
Het Select Box component verwacht twee waardes: 
- Een object met de base url naar de API en het endpoint waaruit de options voor voor de Select Box opgehaald kan worden.
- De key die naar de lijst van options uit het API response wijst.

Voeg de gebruike base url toe aan het `ngsw-config.json` bestand zodat de selectbox ook offline gebruikt kan worden.

### Voorbeeld
De [PokéAPI](https://pokeapi.co/docs/v2) heeft een endpoint genaamd `pokemon` die een lijst van pokemons geeft. De response (in JSON) ziet er zo uit:
```json 
{
  "count": 1302,
  "next": "https://pokeapi.co/api/v2/pokemon?offset=20&limit=20",
  "previous": null,
  "results": [
    {
      "name": "bulbasaur",
      "url": "https://pokeapi.co/api/v2/pokemon/1/"
    },
    {
      "name": "ivysaur",
      "url": "https://pokeapi.co/api/v2/pokemon/2/"
    },
    {
      "name": "venusaur",
      "url": "https://pokeapi.co/api/v2/pokemon/3/"
    },
    ....
  ]
}
```
De lijst van pokemons staan onder `results` waarbij elke pokemon een `name` en een `url` heeft. We willen alle `name`s van de Pokemons ophalen. Om deze API goed in te stellen moet de volgende gegevens gebruikt worden:
```html
<select-box
  [apiUrls]="$any({
  baseApiUrl: 'https://pokeapi.co/api/v2/',
  endpoints: {getOptions: 'pokemon'}})" 
  optionsKey='results.name'>
</select-box>
```
Zoals je in het voorbeeld kunt zien is het mogelijk om geneste objecten op te halen door de keys achter elkaar te zetten en ze te scheiden met een punt.

### Het gebruik van any in `SelectBoxService.convertResponseToArray()`
Het is natuurlijk niet optimaal om `any` te gebruiken. Toch is hiervoor gekozen omdat de response die je terugkrijgt van de api niet bekend is. Je hoeft dus ook niet een model aan te maken voor elke API die je wilt gebruiken. In combinatie met de bovengenoemde syntax leek mij dit een nette en makkelijk te gebruiken manier om dit probleem op te lossen. Ook behoud je dan een stukje herbruikbaarheid van de select-box component. Het nadeel hiervan is wel dat er extra logica bijkomt die complexer is dan wanneer je models gebruikt.

## Vervolgstappen
In het vervolg is het de bedoeling om de volgende functies toe te voegen:
- [ ] Ondersteuning voor filters tijdens het versturen van een request.
- [ ] Ondersteuning voor pagination bij het ophalen van opties.
- [ ] Ondersteuning voor andere response types (XML & plain/text).
- [ ] Ondersteuning voor het toevoegen van afbeeldingen aan de select-box.
- [x] Centralized error handling met alerts.
- [ ] Een secure authorization token. (JWT?)
- [x] Ondersteuning bieden voor API's die een API key nodig hebben.